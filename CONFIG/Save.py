# Copyright (C) 2024 Niedl Andreas
#
# This file is part of Serial-Button-GUI.
#
# Serial-Button-GUI is licensed under the GNU General Public License v3.0.
# You can redistribute it and/or modify it under the terms of the GPL-3.0.
#
# See the LICENSE file for details.

import configparser
from GUI.CommandButton import ButtonColumn, CommandButtons
from HELPER.data_types import Settings_t

class Save():
    def __init__(self) -> None:
        pass

    def SaveConfig(self, data:ButtonColumn, config:Settings_t, file):
        configParser = configparser.ConfigParser(interpolation=None)
        
        #Save CommandButtons
        buttonNr = 0
        for button in data:
            buttonIdent = "Button " + str(buttonNr)
            buttonNr = buttonNr + 1
            try:
                configParser.add_section(buttonIdent)
            except configparser.DuplicateSectionError:
                pass
            buttonText = button.GetText()
            command = button.GetCommand()
            color = button.GetColor()
            configParser.set(buttonIdent, "Name", buttonText)
            configParser.set(buttonIdent, "Command", command)
            configParser.set(buttonIdent, "Color", color)
        configIdent = "Config"
        configParser.add_section(configIdent)
        configParser.set(configIdent, "Baudrate", str(config.baudrate.get()))
        configParser.set(configIdent, "NL", str(config.newLine.get()))
        configParser.set(configIdent, "CR", str(config.curReturn.get()))

        with open(file, "w") as config_file:
            configParser.write(config_file)