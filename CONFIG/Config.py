# Copyright (C) 2024 Niedl Andreas
#
# This file is part of Serial-Button-GUI.
#
# Serial-Button-GUI is licensed under the GNU General Public License v3.0.
# You can redistribute it and/or modify it under the terms of the GPL-3.0.
#
# See the LICENSE file for details.

import configparser
from CONFIG.Load import Load
from CONFIG.Save import Save
from tkinter import filedialog
from HELPER.data_types import Settings_t

class Config ():
    def __init__(self) -> None:
        self.__load = Load()
        self.__save = Save()
        self.currentFile = None


    def SaveDataAs(self, data, config:Settings_t):
        state, self.currentFile = self.__SetFilePath()
        if state == True:
            self.__save.SaveConfig(data=data, config=config, file=self.currentFile)

    def SaveData(self, data, config:Settings_t):
        if not self.currentFile == None:
            self.__save.SaveConfig(data=data, config=config, file=self.currentFile)

    def LoadData(self):
        state, self.currentFile = self.__GetFilePath()
        if state == True:
            return self.__load.LoadConfig(file=self.currentFile)
        return None

    def __GetFilePath(self):
        file = filedialog.askopenfile(mode='r', filetypes=[('ini Files', '*.ini')])
        if file == None:
            return False, file

        return True, file.name

    def __SetFilePath(self):
        file = filedialog.asksaveasfile(filetypes=[('ini Files', '*.ini')], defaultextension=".ini")
        if file == None:
            return False, file

        return True, file.name