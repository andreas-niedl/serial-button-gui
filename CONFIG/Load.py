# Copyright (C) 2024 Niedl Andreas
#
# This file is part of Serial-Button-GUI.
#
# Serial-Button-GUI is licensed under the GNU General Public License v3.0.
# You can redistribute it and/or modify it under the terms of the GPL-3.0.
#
# See the LICENSE file for details.

import configparser
from HELPER.data_types import CommandButton_t, Settings_t

class Load ():
    def __init__(self) -> None:
        self.buttonsArray = []
        self.config = Settings_t()

    def LoadConfig(self, file):
        config = configparser.ConfigParser(interpolation=None)
        config.read(file)
        self.buttonsArray.clear()
        
        
        for currSection in config.sections():
            if "Button" in currSection:
                button = CommandButton_t()
                button.text.set(config.get(section=currSection, option="name"))
                button.command.set(config.get(section=currSection, option="command"))
                try:
                    color = config.get(section=currSection, option="color")
                except: #compatibility to old .ini files without row "color"
                    color = ""
                button.color.set(color)
                self.buttonsArray.append(button)
            if "Config" in currSection:
                self.config.baudrate.set(config.get(section=currSection, option="baudrate"))
                self.config.newLine.set(config.get(section=currSection, option="nl"))
                self.config.curReturn.set(config.get(section=currSection, option="cr"))
                
        return self.buttonsArray, self.config

class config_t():
    def __init__(self, name = "", command = "") -> None:
        self.name = name
        self.command = command