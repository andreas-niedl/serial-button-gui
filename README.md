# Serial Button GUI

<div style="text-align: center;">
    <img src="./media/PIC_Serial-Button-Gui-Main-Window_R01_2024-09-02.png" alt="MainWindow" width="600" />
</div>

## Was leistet die Serial Button GUI?
Die Serial Button GUI ist ein leistungsstarkes Werkzeug zur Verwaltung serieller Kommunikation. Es ermöglicht das Senden und Empfangen von Befehlen über eine serielle Schnittstelle. Ein herausragendes Merkmal dieser Anwendung ist die Möglichkeit, benutzerdefinierte Schaltflächen (Buttons) zu erstellen, die mit spezifischen seriellen Befehlen verknüpft sind. Jeder Button kann individuell benannt und so konfiguriert werden, dass er bei Betätigung einen zugeordneten seriellen Befehl überträgt. Dies erleichtert die Steuerung und Automatisierung von seriellen Geräten durch eine intuitive, visuelle Benutzeroberfläche.

## Unterstützte Betriebsysteme:
* Microsoft Windows

### Branches
* **main:** Beinhaltet ausschließlich den Quellcode. Das ausführbare Programm kann mit der Python-Erweiterung *PyInstaller* erstellt werden.
* **executable_Vx.x:** Die ausführbare Datei (.exe) befindet sich im Verzeichnis ./dist.

## Kurzübersicht

<div style="text-align: center;">
    <img src="./media/PIC_Serial-Button-Gui-Main-Window-Parts_R01_2024-09-02.png" alt="MainWindow" width="600" />
</div>

1. Serial Port - Port auswählen, öffnen und schließen
2. Serial Port - Einstellungen (Baudrate, Steuerzeichen)
3. Konfiguration laden und speichern
4. Serielle Ausgabe
5. Buttons - erstellen, bearbeiten und verschieben
6. Einmaliges versenden serieller Kommandos
7. Senden wiederholen alle X Sekunden

## Quick Guide

1. Seriellen Port auswählen ("Scan" aktualisiert die Port-Liste)
2. In "Settings" die Baudrate und Steuerzeichen konfigurieren.
3. Port öffnen mit "Open"
4. Neuen Button mit "Add Button" erzeugen:
<div style="text-align: left;">
    <img src="./media/PIC_Serial-Button-Gui-Button-Window_R01_2024-09-02.png" alt="ButtonWindow" width="250" />
</div>

* Namen vergeben
* Seriellen Befehl eintragen
* Farbe einstellen (Optional)
5. Button drücken um den Befehl zu senden

# Detailierte Berschreibung

## Serial Port

### Port wählen
<div style="text-align: left;">
    <img src="./media/PIC_Serial-Button-Gui-Serial-Port-List_R01_2024-09-02.png" alt="SelectPort" width="250" />
</div>
Wählen Sie den geeigneten Port aus dem Dropdown-Menü aus. Falls nicht alle verfügbaren Ports angezeigt werden, können Sie die Port-Liste durch Klicken auf die Schaltfläche "Scan" aktualisieren.

### Settings - Baudrate und Steuerzeichen
<div style="text-align: left;">
    <img src="./media/PIC_Serial-Button-Gui-Settings_R01_2024-09-02.png" alt="SettingsWindow" width="250" />
</div>
Die Baudrate und die Steuerzeichen können konfiguriert werden. Diese Einstellungen werden zusammen mit der Konfiguration in der INI-Datei gespeichert.

### Öffnen
<div style="text-align: left;">
    <img src="./media/PIC_Serial-Button-Gui-Serial-Port-Open_R01_2024-09-02.png" alt="PortOpen" width="250" />
</div>
Öffnet den Port.

### Schließen
<div style="text-align: left;">
    <img src="./media/PIC_Serial-Button-Gui-Serial-Port-Close_R01_2024-09-02.png" alt="PortClose" width="250" />
</div>
Schließt den Port.

## Konfiguration
<div style="text-align: left;">
    <img src="./media/PIC_Serial-Button-Gui-Config-Buttons_R01_2024-09-02.png" alt="Config" width="250" />
</div>

"Save Config" oder "Save Config As" speichert die aktuellen Buttons und Einstellungen in einer externen INI-Datei. Der Dateiname kann dabei frei gewählt werden.

"Load Config" lädt eine zuvor gespeicherte Konfiguration zurück in das Programm.

## Buttons

### Erstellen
<div style="text-align: left;">
    <img src="./media/PIC_Serial-Button-Gui-Button-Add_R01_2024-09-02.png" alt="CreateButton" width="250" />
</div>
<div style="text-align: left;">
    <img src="./media/PIC_Serial-Button-Gui-Button-Window_R01_2024-09-02.png" alt="CreateButton" width="250" />
</div>
Mit der Schaltfläche "AddButton" kann ein neuer Button erstellt werden. Dieser wird am Ende der Button-Liste eingefügt. Jeder neue Button benötigt einen Namen und einen zugehörigen seriellen Befehl; die Farbe des Buttons ist optional. Es können maximal 24 Buttons erstellt werden.

### Bearbeiten
<div style="text-align: left;">
    <img src="./media/PIC_Serial-Button-Gui-Button-edit_R01_2024-09-02.png" alt="EditButton" width="250" />
</div>
Öffnet ein Dialogfenster zur Anpassung von Name, Befehl und Farbe des Buttons.

### Löschen
<div style="text-align: left;">
    <img src="./media/PIC_Serial-Button-Gui-Button-delete_R01_2024-09-02.png" alt="DeleteButton" width="250" />
</div>
Löscht den ausgewählten Button.

### Verschieben
<div style="text-align: left;">
    <img src="./media/PIC_Serial-Button-Gui-Button-move_R01_2024-09-02.png" alt="MoveButton" width="250" />
</div>
Verschiebt den Button um eine Position nach oben oder nach unten.

## Serielle Ausgabe

### Ausgabefenster
<div style="text-align: left;">
    <img src="./media/PIC_Serial-Button-Gui-Seriell-Output_R01_2024-09-02.png" alt="SerialOutput" width="250" />
</div>
Hier werden alle empfangenen Zeichen vom seriellen Port angezeigt.

### Leeren
<div style="text-align: left;">
    <img src="./media/PIC_Serial-Button-Gui-Clear-Output_R01_2024-09-02.png" alt="ClearOutput" width="250" />
</div>
Leert das Ausgabefenster.

## Wiederholen

### Einmaliges Senden
<div style="text-align: left;">
    <img src="./media/PIC_Serial-Button-Gui-Repeat-send-once_R01_2024-09-02.png" alt="SendOnce" width="250" />
</div>
Wenn diese Einstellung aktiviert ist, wird der Befehl beim Drücken eines Buttons nur einmal gesendet.

### Periodisches Senden
<div style="text-align: left;">
    <img src="./media/PIC_Serial-Button-Gui-Repeat-send_R01_2024-09-02.png" alt="RepeatEvery" width="250" />
</div>
Wenn diese Einstellung aktiviert ist, wird der Befehl beim Drücken eines Buttons alle X Sekunden periodisch gesendet. Der zuletzt gedrückte Button wird dabei kontinuierlich wiederholt.<br>
<br>

# Lizenz
Copyright (C) 2024 Niedl Andreas

This file is part of Serial-Button-GUI.

Serial-Button-GUI is licensed under the GNU General Public License v3.0.

You can redistribute it and/or modify it under the terms of the GPL-3.0.
See the LICENSE file for details.
