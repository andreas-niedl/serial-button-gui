# Copyright (C) 2024 Niedl Andreas
#
# This file is part of Serial-Button-GUI.
#
# Serial-Button-GUI is licensed under the GNU General Public License v3.0.
# You can redistribute it and/or modify it under the terms of the GPL-3.0.
#
# See the LICENSE file for details.

import tkinter as tk

class CommandButton_t():
    def __init__(self, text="", command="", color="SystemButtonFace") -> None:
        self.text = tk.StringVar()
        self.text.set(text)
        self.command = tk.StringVar()
        self.command.set(command)
        self.color = tk.StringVar()
        self.color.set(color) #default grey color = "SystemButtonFace"
        
class Settings_t():
    def __init__(self, baud=9600, newLine=False, curReturn=False) -> None:
        self.baudrate = tk.IntVar()
        self.baudrate.set(baud)
        self.newLine = tk.IntVar()
        self.newLine.set(newLine)
        self.curReturn = tk.IntVar()
        self.curReturn.set(curReturn)