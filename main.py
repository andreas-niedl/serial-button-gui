# Copyright (C) 2024 Niedl Andreas
#
# This file is part of Serial-Button-GUI.
#
# Serial-Button-GUI is licensed under the GNU General Public License v3.0.
# You can redistribute it and/or modify it under the terms of the GPL-3.0.
#
# See the LICENSE file for details.

from GUI.Gui import Gui

def main():

    gui = Gui()
    while True:
        gui.Update()

main()


#Create executable (Terminal Command): pyinstaller --onefile main.py --name "Serial Button GUI v0.7"
# 

