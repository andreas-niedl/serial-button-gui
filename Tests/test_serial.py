import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

#from GUI.Button import Button
#from GUI.CommandButton import CommandButtons, ButtonColumn
from GUI.Button import Button
from GUI.Window import Window, SettingsWindow
from SERIAL.Serial import SerialPort
import tkinter as tk
from HELPER.data_types import Settings_t
from GUI.Label import Label

class Gui():
    def __init__(self) -> None:
        
        self.window = Window(   title="test buttons",
                                geometry="1000x700") 
        self.settingsWindow = None
        self.settings = Settings_t(baud=500000, newLine=True, curReturn=True) 
        
        settingsButton = Button(window=self.window,
                                text="Settings",
                                command_func=self.OpenSettingsWindow,
                                width=20)

                                
        settingsButton.Place(  column=10,
                                row=1,
                                padx=5,
                                sticky="ew")
        
        
        
        #----------------Serial Port Objects------------------

        self.__serial = SerialPort()
        self.__serial.SetSettings(settings=self.settings)
        self.__serial.SetPrinterFunc(print_func=self.PrintRx)
        self.__serialState = tk.Variable()
        # Labels:
        labelSerial = Label(    window=self.window,
                                text="Serial Port:",
                                font=("times", 12, "bold"))
        labelSerial.Place(  column=1,
                            row=1)

        self.__labelSerialState = Label(    window=self.window,
                                            textvariable=self.__serialState,
                                            font=("times", 12))
        self.__labelSerialState.Place(  column=4,
                                        row=1)
        

        # Buttons:
        buttonClosePort = Button(   window=self.window,
                                text="Close",
                                command_func=lambda state=False: self.__SetPortState(state),
                                width=8)
        buttonClosePort.Place(  column=2,
                                row=1,
                                padx=5,
                                sticky="ew")

        buttonOpen = Button(    window=self.window,
                                text="Open",
                                command_func=lambda state=True: self.__SetPortState(state),
                                width=8)
        buttonOpen.Place(  column=3,
                            row=1,
                            padx=5,
                            sticky="ew")
        
        # Select Port:
        selectPort = tk.StringVar(master=self.window, value="Select port...")
        selectPort.trace_add("write", lambda *args: self.__serial.SetPort(selectPort.get()))
        availablePorts = self.__serial.GetAvailablePorts()

        self.serialPortDropDown = tk.OptionMenu(    self.window,
                                                    selectPort,
                                                    *availablePorts.keys())
        self.serialPortDropDown.grid(column=5,
                                     row=1, 
                                     columnspan=3, 
                                     sticky="ew")
        self.__SetPortState(False)
        
        sendMeasage = Button(    window=self.window,
                                text="Send",
                                command_func=lambda data="TM901#0: *IDN?": self.__serial.Send(data),
                                width=8)
        sendMeasage.Place(  column=3,
                            row=10,
                            padx=5,
                            sticky="ew")
        
        #self.window.mainloop()   

    def OpenSettingsWindow(self):
        self.settingsWindow = SettingsWindow(window=self.window, 
                                             settings=self.settings, 
                                             closeWindow_func=self.CloseSettingsWindow)

    def CloseSettingsWindow(self, type, settings:Settings_t = None):
        if type == "save":
            self.settings.baudrate.set(settings.baudrate.get())
            self.settings.newLine.set(settings.newLine.get())
            self.settings.curReturn.set(settings.curReturn.get())
            self.__serial.SetSettings(self.settings)
        self.settingsWindow.Close()

    def __SetPortState(self, state):
            if state == True:
                self.__serial.Open()
            else:
                self.__serial.Close()

            if self.__serial.GetState() == True:
                self.__serialState.set("State: open")
                self.__labelSerialState.config(fg="green")
            else:
                self.__serialState.set("State: close")
                self.__labelSerialState.config(fg="red")
                
    def PrintRx(self, rx_data):
        rx_data = rx_data.replace("\r", "")    #del \r  
        rx_data = rx_data.replace("\n", "")    #del \n   
        print(rx_data)
           
    def Update(self):
        self.window.update()
        
        #self.__serial.PollRx()
            


def main():
    
    gui = Gui()
    gui.window.mainloop()
    # while True:
    #     gui.Update()
    
    
    

    
main()