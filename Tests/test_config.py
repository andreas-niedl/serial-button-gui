import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

from GUI.Button import Button
from GUI.CommandButton import CommandButtons, ButtonColumn
from GUI.Window import Window
from CONFIG.Config import Config


window = Window(    title="test buttons",
                    geometry="1000x700")

commandButtons = []
config = Config()

def LoadConfig():
    data = config.LoadData()
    if data == None: 
        return
    

#----------------Config Objects------------------
buttonSaveConfig = Button(  window=window,
                            text="Save Config",
                            command_func=lambda array=commandButtons: config.SaveData(array),
                            width=10)
buttonSaveConfig.Place( column=1,
                        row=2,
                        sticky="ew")                           
buttonSaveConfigAs = Button(    window=window,
                                text="Save Config As",
                                command_func=lambda array=commandButtons: config.SaveDataAs(array),
                                width=10)
buttonSaveConfigAs.Place(   column=2,
                            row=2,
                            sticky="ew")
buttonLoadConfig = Button(  window=window,
                            text="Load Config",
                            command_func=LoadConfig,
                            width=10)
buttonLoadConfig.Place(     column=3,
                            row=2,
                            sticky="ew")

def main():
   
    
    
    window.mainloop()
    

    
    
main()