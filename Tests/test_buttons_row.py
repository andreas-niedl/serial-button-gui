import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

#from GUI.Button import Button
from GUI.CommandButton import CommandButtons, ButtonColumn
from GUI.Window import Window

buttonColumn = ButtonColumn()
window = Window(    title="test buttons",
                        geometry="1000x700")                                        

def main():
    CreateButtonGroup()
    buttonColumn.Place(column=1, row=1)
    
    
    window.mainloop()
    
    
def CreateButtonGroup():
    for name in range(5):
        newButton = CommandButtons(window=window, text=str(name))
                            
        # newButton.Place(row=name+1,
        #                 column=1)
        buttonColumn.Add(newButton)
        
        
    
main()