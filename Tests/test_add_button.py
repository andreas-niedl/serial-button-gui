import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

from GUI.Button import Button
from GUI.CommandButton import CommandButtons, ButtonColumn
from GUI.Window import Window

window = Window(    title="test buttons",
                    geometry="1000x700")

buttonColumn = ButtonColumn()

buttonAddButton = Button(  window=window,
                                text="Add Button",
                                command_func= lambda window=window: buttonColumn.New(window=window),
                                width=10)
buttonAddButton.Place(  column=0,
                        row=0,
                        pady=5,
                        columnspan=2,
                        sticky="ew")

def main():
    # CreateButtonGroup()
    buttonColumn.Place(column=3, row=1)
    
    
    window.mainloop()
    

    
main()