# Copyright (C) 2024 Niedl Andreas
#
# This file is part of Serial-Button-GUI.
#
# Serial-Button-GUI is licensed under the GNU General Public License v3.0.
# You can redistribute it and/or modify it under the terms of the GPL-3.0.
#
# See the LICENSE file for details.

import tkinter as tk
from GUI.Window import EditWindow
from GUI.Button import Button
from HELPER.data_types import CommandButton_t

class CommandButtons(): #Bottonsrow
    def __init__(self, window, values:CommandButton_t = None) -> None:
        self.window = window
        self.values = CommandButton_t()
        if values != None:
            self.values.command.set(values.command.get())
            self.values.text.set(values.text.get())
            self.values.color.set(values.color.get())
            
        self.commandButton = Button(    window=window, 
                                        text=self.values.text.get(),
                                        width=30, 
                                        height=0, 
                                        font=("", 10, "bold"))
        if values.color.get() != "":
            self.commandButton.SetColor(color=self.values.color)
        
        self.editButton = Button(   window=window, 
                                    text="edit", 
                                    width=5, 
                                    height=0)
        self.deleteButton = Button( window=window, 
                                    text="delete", 
                                    width=5, 
                                    height=0)
        self.upButton = Button(     window=window, 
                                    text="up", 
                                    width=5, 
                                    height=0)
        self.downButton = Button(   window=window, 
                                    text="down", 
                                    width=5, 
                                    height=0)
        self.editWindow = None
        self.deleteButton_func = None
        self.commandButton_func = None

    def SetCommand(self, command):
        self.values.command.set(command)
        
    def GetCommand(self):
        return self.values.command.get()
        
    def GetText(self):
        return self.commandButton.text.get()
    
    def GetColor(self):
        color = self.commandButton.GetColor()
        if color == "SystemButtonFace": #default grey color
            return ""
        
        return color
        
    def Place(self, column, row):
        startColumn = column
        self.commandButton.Place(column=startColumn, row=row, columnspan=1, sticky="n", pady=1)
        self.editButton.Place(column=startColumn + 1, row=row, columnspan=1, sticky="n", pady=1)
        self.editButton.SetCommandFunc(lambda type = "Edit Button":self.OpenEditWindow(type=type))
        self.upButton.Place(column=startColumn + 2, row=row, columnspan=1, sticky="n", pady=1)
        self.downButton.Place(column=startColumn + 3, row=row, columnspan=1, sticky="n", pady=1)
        self.deleteButton.Place(column=startColumn + 4, row=row, columnspan=1, sticky="n", pady=1)
        
    def Delete(self):
        self.commandButton.Delete()
        self.editButton.Delete()
        self.deleteButton.Delete()
        self.upButton.Delete()
        self.downButton.Delete()
        
    def Configure(self, commandButton_func = None, upButton_func = None, downButton_func = None, deleteButton_func = None):
        if(commandButton_func != None):
            self.commandButton_func = commandButton_func
            self.commandButton.SetCommandFunc(command_func=self.__SendCommand)
        if(upButton_func != None):
            self.upButton.SetCommandFunc(command_func=upButton_func)
        if(downButton_func != None):
            self.downButton.SetCommandFunc(command_func=downButton_func)
        if(deleteButton_func != None):
            self.deleteButton_func = deleteButton_func
            self.deleteButton.SetCommandFunc(command_func=self.deleteButton_func)
            
    def OpenEditWindow(self, type):
        buttonValues = CommandButton_t( text=self.commandButton.text.get(),
                                        command=self.values.command.get(),
                                        color=self.commandButton.GetColor())
        self.editWindow = EditWindow(   window=self.window,
                                        type=type, 
                                        closeWindow_func=self.__CloseEditWindow,
                                        buttonValues=buttonValues)
        
    def __CloseEditWindow(self, buttonValues:CommandButton_t = None, type="save"):
        if type == "save":
            self.commandButton.text.set(buttonValues.text.get())
            command = buttonValues.command.get()
            self.values.command.set(buttonValues.command.get())
            self.commandButton.SetColor(color=buttonValues.color)
        elif type == "newCancel":
            self.deleteButton_func()
            
        self.editWindow.Close()
        
    def __SendCommand(self):
        self.commandButton_func(self.values.command.get())
        
                                      
class ButtonColumn():
    def __init__(self, serialTx_func) -> None:
        self.serialTx_func = serialTx_func
        self.__buttonColumn = []
        self.__column = 0
        self.__row = 0

    def Place(self, column, row):
        self.__column = column
        self.__row = row
        self.__Update()
        
    def __Update(self):
        pos = 1
        for buttonRow in self.__buttonColumn:
            buttonRow.Place(column=self.__column, row=self.__row + pos)
            pos = pos + 1

    def Delete(self, button:Button):
        button.Delete()
        self.__buttonColumn.remove(button)
        
    def DeleteAll(self):
        for button in self.__buttonColumn:
            button.Delete()
        self.__buttonColumn.clear()
        
    def New(self, window, buttonValues:CommandButton_t = None, type="AddButton"):
        # max numbers of Buttons
        if len(self.__buttonColumn) >= 24:
            return
        values = CommandButton_t()
        if buttonValues != None:
            values.text.set(buttonValues.text.get())
            values.command.set(buttonValues.command.get())
            values.color.set(buttonValues.color.get())
        #New from AddButton
        if type == "AddButton":
            newButton = CommandButtons( window=window,
                                        values=values)
            newButton.OpenEditWindow(type="Add Button")
            
        else: # New from .ini
            newButton = CommandButtons(window=window,
                                       values=values)
        self.Add(newButton)
        self.__Update()

    def Add(self, button:CommandButtons):
        button.Configure(   commandButton_func=self.serialTx_func,
                            upButton_func=lambda selfButton=button: self.MoveUp(selfButton),
                            downButton_func=lambda selfButton=button: self.MoveDown(selfButton),
                            deleteButton_func=lambda selfButton=button: self.Delete(selfButton))
        self.__buttonColumn.append(button)
        
    def MoveUp(self, button):
        buttonIdx = self.__buttonColumn.index(button)
        if buttonIdx == 0:
            return
        saveButton = self.__buttonColumn[buttonIdx]
        self.__buttonColumn[buttonIdx] = self.__buttonColumn[buttonIdx - 1]
        self.__buttonColumn[buttonIdx - 1] = saveButton
        
        self.__Update()
        
    def MoveDown(self, button):
        buttonIdx = self.__buttonColumn.index(button)
        if buttonIdx == len(self.__buttonColumn) - 1:
            return
        saveButton = self.__buttonColumn[buttonIdx]
        self.__buttonColumn[buttonIdx] = self.__buttonColumn[buttonIdx + 1]
        self.__buttonColumn[buttonIdx + 1] = saveButton
        
        self.__Update()
        
    def GetButtonColumnArray(self):
        return self.__buttonColumn