# Copyright (C) 2024 Niedl Andreas
#
# This file is part of Serial-Button-GUI.
#
# Serial-Button-GUI is licensed under the GNU General Public License v3.0.
# You can redistribute it and/or modify it under the terms of the GPL-3.0.
#
# See the LICENSE file for details.

from GUI.CommandButton import ButtonColumn
from GUI.Button import Button
from GUI.Window import Window, SettingsWindow
from SERIAL.Serial import SerialPort
import tkinter as tk
from HELPER.data_types import Settings_t
from GUI.Label import Label
from GUI.Textfield import Textfield
from CONFIG.Config import Config
from GUI.Entry import Entry

COMMAND_BUTTON_START_COLUMN = 22
COMMAND_BUTTON_START_ROW = 1

class Gui():
    def __init__(self) -> None:
        
        #----------------Main Window------------------
        self.window = Window(   title="Serial Button GUI V0.7 - GNU GPL v3.0 License",
                                geometry="1300x850")
        
        self.settings = Settings_t(baud=500000, newLine=True, curReturn=True) #Default Settings 
        self.txRepeatTime = tk.StringVar(value="1")
        self.__serial = SerialPort(txRepeatTimeVar=self.txRepeatTime)
        self.__serial.SetSettings(settings=self.settings)
        self.__serialState = tk.Variable()
        self.availablePortList = []
        
        self.config = Config()
        
        #Labels
        labelSerial = Label(    window=self.window,
                                text="Serial Port:",
                                font=("", 10, "bold"))
        labelSerial.Place(      column=1,
                                row=1)

        self.__labelSerialState = Label(    window=self.window,
                                            textvariable=self.__serialState,
                                            font=("times", 12))
        self.__labelSerialState.Place(      column=4,
                                            row=1)
        
        labeltxEntry = Label(   window=self.window,
                                text="Tx:",
                                font=("", 10, "bold"))
        labeltxEntry.Place(     column=2,
                                row=106,
                                sticky="e")
        
        labelTxMode = Label(   window=self.window,
                                text="Send Mode:",
                                font=("", 10, "bold"))
        labelTxMode.Place(     column=2,
                                row=107,
                                sticky="e",
                                pady=10)
        
        labelTxModeUnit = Label(   window=self.window,
                                text="s",
                                font=("", 10, "bold"))
        labelTxModeUnit.Place(  column=5,
                                row=107,
                                sticky="W",
                                pady=1, columnspan=1)
        
        # dummyLabel = Label(     window=self.window,
        #                         text="",
        #                         font=("times", 12, "bold"),
        #                         width=60)
        # dummyLabel.Place(       column=22,
        #                         row=1,
        #                         columnspan=6)
        
        #Buttons
        self.commandButtonColumn = ButtonColumn(serialTx_func=self.__serial.Send) 
        self.commandButtonColumn.Place( column=COMMAND_BUTTON_START_COLUMN,
                                        row=COMMAND_BUTTON_START_ROW)
        
        buttonClosePort = Button(   window=self.window,
                                    text="Close",
                                    command_func=lambda state=False: self.__SetPortState(state),
                                    width=11)
        buttonClosePort.Place(      column=2,
                                    row=1,
                                    padx=5,
                                    sticky="ew")

        buttonOpenPort = Button(    window=self.window,
                                    text="Open",
                                    command_func=lambda state=True: self.__SetPortState(state),
                                    width=11)
        buttonOpenPort.Place(       column=3,
                                    row=1,
                                    padx=5,
                                    sticky="ew")
        
        buttonAddButton = Button(   window=self.window,
                                    text="Add Button",
                                    command_func= lambda window=self.window: self.commandButtonColumn.New(window=window),
                                    width=30)
        buttonAddButton.Place(      column=22,
                                    row=1,
                                    #padx=5,
                                    columnspan=1,
                                    sticky="ew")
        
        buttonScanPorts = Button( window=self.window,
                                    text="Scan",
                                    command_func=self.UpdateAvailablePorts,
                                    width=4)
        buttonScanPorts.Place(   column=12,
                                    row=1,
                                    padx=1,
                                    sticky="e")
        
        settingsButton = Button(    window=self.window,
                                    text="Settings",
                                    command_func=self.OpenSettingsWindow,
                                    width=6)                   
        settingsButton.Place(       column=13,
                                    row=1,
                                    padx=5,
                                    sticky="e")
        
        buttonLoadConfig = Button(  window=self.window,
                                    text="Load Config",
                                    command_func=self.LoadConfigData,
                                    width=11)
        buttonLoadConfig.Place(     column=1,
                                    row=2,
                                    padx=5,
                                    sticky="ew")
        
        buttonSaveConfig = Button(  window=self.window,
                                    text="Save Config",
                                    command_func=self.SaveData,
                                    width=11)
        buttonSaveConfig.Place(     column=2,
                                    row=2,
                                    padx=5,
                                    sticky="ew")  
                                 
        buttonSaveConfigAs = Button(window=self.window,
                                    text="Save Config As",
                                    command_func=self.SaveDataAs,
                                    width=11)
        buttonSaveConfigAs.Place(   column=3,
                                    row=2,
                                    padx=5,
                                    sticky="ew")
        
        
        #Entrys
        self.entrySerialTx = Entry( window=self.window, width=2)
        self.entrySerialTx.Place(   column=3,
                                    row=106,
                                    columnspan=3)
        
        self.entryRepeatTime = Entry(   window=self.window, 
                                        textvariable=self.txRepeatTime, 
                                        width=1)
        self.entryRepeatTime.Place(     column=4,
                                        row=107,
                                        columnspan=1, 
                                        padx=3)
        
        #Dropdown
        self.selectPort = tk.StringVar(value="Select port...")
        self.ScanAvailablePorts()
        self.serialPortDropDown = tk.OptionMenu(    self.window,
                                                    self.selectPort,
                                                    *self.availablePortList,
                                                    command=self.SelectedPort)
        
        self.serialPortDropDown.configure(width=20, anchor="w")
        self.serialPortDropDown.grid(   column=5,
                                        row=1, 
                                        padx=5,
                                        columnspan=8, 
                                        sticky="w")
        self.__SetPortState(False)
        
        selectSendMode = tk.StringVar(master=self.window, value="Send once")
        sendModeList = ["Send once", "Repeat every"]
        self.sendModeDropDown = tk.OptionMenu(  self.window,
                                                selectSendMode,
                                                *sendModeList, 
                                                command=self.ChangeSendMode)
        self.sendModeDropDown.configure(width=12)
        self.sendModeDropDown.grid(   column=3,
                                        row=107, 
                                        padx=1,
                                        columnspan=1, 
                                        sticky="e")
        
        #Textfield
        self.rxTextfield = Textfield(   window=self.window,
                                        width=100,
                                        height=40,
                                        font=("Times", 11))
        self.rxTextfield.Place(         column=1,
                                        row=3,
                                        pady=5,
                                        padx=5,
                                        columnspan=21,
                                        rowspan=100)
        
        buttonClearTerminal = Button(   window=self.window,
                                        text="Clear",
                                        command_func=self.rxTextfield.Clear,
                                        width=11)
        buttonClearTerminal.Place(      column=1,
                                        row=106,
                                        padx=5,
                                        sticky="ew")
        
        buttonSendTxEntry = Button( window=self.window,
                                    text="Send",
                                    command_func=self.SendTxEntry,
                                    width=11)
        buttonSendTxEntry.Place(   column=11,
                                    row=106,
                                    padx=2,
                                    sticky="w")
        
        buttonCancelCommand = Button(   window=self.window,
                                        text="Cancel Send",
                                        command_func=self.__serial.SendCancelCommand,
                                        width=11)
        buttonCancelCommand.Place(      column=12,
                                        row=106,
                                        padx=2,
                                        sticky="w")
        
        self.__serial.SetPrinterFunc(print_func=self.rxTextfield.Print)
        
        #----------------Settings Window------------------
        self.settingsWindow = None
        
        self.ChangeSendMode(selected="Send once")
        
         

    def OpenSettingsWindow(self):
        self.settingsWindow = SettingsWindow(window=self.window, 
                                             settings=self.settings, 
                                             closeWindow_func=self.CloseSettingsWindow)

    def CloseSettingsWindow(self, type, settings:Settings_t = None):
        if type == "save":
            self.settings.baudrate.set(settings.baudrate.get())
            self.settings.newLine.set(settings.newLine.get())
            self.settings.curReturn.set(settings.curReturn.get())
            self.__serial.SetSettings(self.settings)
        self.settingsWindow.Close()

    def __SetPortState(self, state):
            if state == True:
                self.__serial.Open()
            else:
                self.__serial.Close()
 
    def __UpdatePortState(self):
        try:
            if self.__serial.GetState() == True:
                    self.__serialState.set("State: open")
                    self.__labelSerialState.config(fg="green")
            else:
                self.__serialState.set("State: close")
                self.__labelSerialState.config(fg="red")
        except:
            pass
                
    def PrintRx(self, rx_data):
        rx_data = rx_data.replace("\r", "")    #del \r  
        rx_data = rx_data.replace("\n", "")    #del \n   
        print(rx_data)
           
    def Update(self):
        try:
            self.window.update()
        except Exception as e:
            print(e)
            exit()
        self.__serial.PollRx()
        self.__serial.CheckNewRepeatTime()
        self.__serial.PollRepeatSend()
        self.__UpdatePortState()
        
        
    def LoadConfigData(self):
        data, config = self.config.LoadData()
        if data != None:
            self.commandButtonColumn.DeleteAll()
            for buttonValues in data:
                self.commandButtonColumn.New(window=self.window, type="ini", buttonValues=buttonValues)
                
        self.settings = config
        self.__serial.SetSettings(settings=self.settings)
      
    def SendTxEntry(self):
        entryTxVar = self.entrySerialTx.GetTextVariable()
        self.__serial.Send(message=entryTxVar.get())     
        self.entrySerialTx.Clear() 
        
    def SaveData(self):
        self.config.SaveData(data=self.commandButtonColumn.GetButtonColumnArray(),
                             config=self.settings)

    def SaveDataAs(self):
        self.config.SaveDataAs( data=self.commandButtonColumn.GetButtonColumnArray(),
                                config=self.settings)
        
    def ChangeSendMode(self, selected):
        if selected == "Send once":
            self.__serial.SetRepeatSend(state=False)
            disableVar = tk.StringVar()
            self.entryRepeatTime.configure(state="disabled", textvariable=disableVar)

        elif selected == "Repeat every":
            self.__serial.SetRepeatSend(state=True)
            self.entryRepeatTime.configure(state="normal", textvariable=self.txRepeatTime)

    def ScanAvailablePorts(self):
        self.availablePortList = self.__serial.GetAvailablePorts().keys()
        
    def UpdateAvailablePorts(self):
        self.ScanAvailablePorts()
        
        menu = self.serialPortDropDown["menu"]
        menu.delete(0, "end")
        for port in self.availablePortList:
            menu.add_command(label=port, 
                             command=lambda port=port: self.SelectedPort(port=port))
            
    def SelectedPort(self, port):
        self.selectPort.set(port)
        self.__serial.SetPort(portName=port)