# Copyright (C) 2024 Niedl Andreas
#
# This file is part of Serial-Button-GUI.
#
# Serial-Button-GUI is licensed under the GNU General Public License v3.0.
# You can redistribute it and/or modify it under the terms of the GPL-3.0.
#
# See the LICENSE file for details.

import tkinter as tk

class Label(tk.Label):
    def __init__(self, window, text = "", textvariable=None, width = 10, font=("", 10, "")) -> None:
       
        tk.Label.__init__(   self,
                            master=window,
                            width=width,
                            font=font)

        self.__text = tk.StringVar(value=text)
        if textvariable == None:
            self.configure( textvariable=self.__text)
        else:
            self.configure( textvariable=textvariable)

    def Place(self, column, row, columnspan = 1, sticky = "ew", padx = 0, pady = 0):
        self.grid(  column=column,
                    row=row,
                    columnspan=columnspan,
                    sticky=sticky,
                    padx=padx,
                    pady=pady)

    def Delete(self):
        self.destroy()

    def SetText(self, name):
        self.__text.set(name)

    def GetColumn(self):
        return self.GetColumn()

    def GetRow(self):
        return self.GetRow()