# Copyright (C) 2024 Niedl Andreas
#
# This file is part of Serial-Button-GUI.
#
# Serial-Button-GUI is licensed under the GNU General Public License v3.0.
# You can redistribute it and/or modify it under the terms of the GPL-3.0.
#
# See the LICENSE file for details.

import tkinter as tk
import tkinter.font as fnt

class Button(tk.Button):
    def __init__(   self, 
                    window, 
                    text = "",
                    color = "SystemButtonFace", #default grey color
                    command_func = None,
                    width = 1,
                    height = 1,
                    font=("", 10, "")) -> None:
        self.__color = color 
        self.window = window
        tk.Button.__init__( self,
                            master=window,
                            width=width,
                            height=height,
                            font=font)
        self.text = tk.StringVar(value=text)
        self.configure( textvariable=self.text,
                        command=command_func,
                        bg=self.__color)
        self.row = 0
        self.column = 0
        
        
    def Place(self, column, row, columnspan = 1, sticky = "ew", padx = 0, pady = 0):
        self.row = row
        self.column = column
        self.grid(  column=self.column,
                    row=self.row,
                    columnspan=columnspan,
                    sticky=sticky,
                    padx=padx,
                    pady=pady)

    def Delete(self):
        self.destroy()

    def SetName(self, text):
        self.text.set(text)

    def SetCommandFunc(self, command_func):
        self.configure(command=command_func)

    def GetColumn(self):
        return self.column

    def GetRow(self):
        return self.row

    def SetColor(self, color):
        self.__color = color.get()
        self.configure(bg=self.__color)
        
    def GetColor(self):
        return self.__color
