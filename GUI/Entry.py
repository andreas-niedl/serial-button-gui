# Copyright (C) 2024 Niedl Andreas
#
# This file is part of Serial-Button-GUI.
#
# Serial-Button-GUI is licensed under the GNU General Public License v3.0.
# You can redistribute it and/or modify it under the terms of the GPL-3.0.
#
# See the LICENSE file for details.

import tkinter as tk

class Entry(tk.Entry):
    def __init__(self, window, textvariable = None, width = 10, font=("", 10, "")) -> None:
        if textvariable == None:
            self.__textvariable = tk.StringVar()
        else:
            self.__textvariable = textvariable
        tk.Entry.__init__(  self,
                            master=window,
                            width=width,
                            font=font,
                            textvariable=self.__textvariable)


    def Place(self, column, row, columnspan = 1, sticky = "ew", padx = 0, pady = 0):
        self.grid(  column=column,
                    row=row,
                    columnspan=columnspan,
                    sticky=sticky,
                    padx=padx,
                    pady=pady)

    def Delete(self):
        self.destroy()

    def GetTextVariable(self):
        return self.__textvariable
    
    def Clear(self):
        self.__textvariable.set("")

    def GetColumn(self):
        return self.GetColumn()

    def GetRow(self):
        return self.GetRow()