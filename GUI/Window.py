# Copyright (C) 2024 Niedl Andreas
#
# This file is part of Serial-Button-GUI.
#
# Serial-Button-GUI is licensed under the GNU General Public License v3.0.
# You can redistribute it and/or modify it under the terms of the GPL-3.0.
#
# See the LICENSE file for details.

import tkinter as tk
from GUI.Button import Button
from HELPER.data_types import CommandButton_t, Settings_t
from GUI.Entry import Entry
from GUI.Label import Label
from tkinter.colorchooser import askcolor

class Window(tk.Tk, tk.Toplevel):
    def __init__(   self, 
                    title, 
                    geometry, 
                    mainWindow = None
                ):
        self.mainWindow = mainWindow
        #create main window
        if mainWindow == None:
            tk.Tk.__init__(self)
        #create sub window   
        else:
            tk.Toplevel.__init__(self, master = mainWindow)
            
        self.title(title)
        self.geometry(geometry)

    def Close(self):
        self.destroy()

    def Show(self):
        if self.mainWindow == None:
            #self.mainloop()
            self.update()

class EditWindow():
    def __init__(self, window, buttonValues:CommandButton_t, type = "Add Button",  closeWindow_func=None) -> None:
        self.type = type
        self.__buttonConfigWindow = Window (    title=type,
                                                geometry="480x180",
                                                mainWindow=window)
        self.__buttonConfigWindow.attributes('-topmost',True) #force configWindow to front
        self.__temp = CommandButton_t(   text=buttonValues.text.get(),
                                        command=buttonValues.command.get(),
                                        color=buttonValues.color.get())
        
        self.closeWindow_func = closeWindow_func
        self.Show()
        
    def Show(self):
        ENTRY_WIDTH = 40
       
        labelName = Label(  window=self.__buttonConfigWindow,
                            text="Name")
        labelName.Place(    column=0,
                            row=0,
                            padx=5,
                            pady=5)
        
        entryName = Entry(  window=self.__buttonConfigWindow,
                            textvariable=self.__temp.text,
                            width=ENTRY_WIDTH)
        entryName.Place(    column=1,
                            row=0,
                            columnspan=2)
        
        labelCom = Label(   window=self.__buttonConfigWindow,
                            text="Command")
        labelCom.Place(     column=0,
                            row=1,
                            padx=5,
                            pady=5)
        
        entryCom = Entry(   window=self.__buttonConfigWindow,
                            textvariable=self.__temp.command,
                            width=ENTRY_WIDTH)
        entryCom.Place(     column=1,
                            row=1,
                            columnspan=2)
        
        labelColor = Label( window=self.__buttonConfigWindow,
                            text="Color:")
        labelColor.Place(   column=0,
                            row=2,
                            padx=5,
                            pady=5)
        
        self.buttonColor = Button(  window=self.__buttonConfigWindow,
                                    text="",
                                    command_func= self.SelectColor,
                                    width=1)
        self.buttonColor.SetColor(  color=self.__temp.color)
        self.buttonColor.Place(     column=1,
                                    row=2,
                                    padx=0,
                                    pady=0)
        
        if self.type == "Add Button":
            addButton = Button( window=self.__buttonConfigWindow,
                                text="Add Button",
                                command_func=lambda type = "save", 
                                                    buttonValues = self.__temp: self.closeWindow_func(  type=type, 
                                                                                                        buttonValues=buttonValues))
                                
            addButton.Place(    column=1,
                                row=5,
                                padx="30",
                                pady="30")
            
            addCancelButton = Button(   window=self.__buttonConfigWindow,
                                        text="Cancel",
                                        command_func=lambda type = "newCancel",
                                                            buttonValues = self.__temp:self.closeWindow_func(type=type,
                                                                                                     buttonValues=buttonValues))
            
        elif self.type == "Edit Button":
            addButton = Button( window=self.__buttonConfigWindow,
                                text="Apply",
                                command_func=lambda type = "save", 
                                                    buttonValues = self.__temp:self.closeWindow_func(   type=type, 
                                                                                                        buttonValues=buttonValues))
            addButton.Place(    column=1,
                                row=5)
            
            addCancelButton = Button(   window=self.__buttonConfigWindow,
                                        text="Cancel",
                                        command_func=lambda type = "cancel":self.closeWindow_func(type=type))

        
        addCancelButton.Place(  column=2,
                                row=5,
                                padx="30",
                                pady="30")
        self.__buttonConfigWindow.Show()
        
    def Close(self):
        self.__buttonConfigWindow.Close()
        
    def SelectColor(self):
        self.__buttonConfigWindow.attributes('-topmost',False)
        color = askcolor(title="Select Button Color")
        self.__buttonConfigWindow.attributes('-topmost',True) #force configWindow to front
        if color[1] == None:
            return
        self.__temp.color.set(color[1])
        self.buttonColor.configure(bg=self.__temp.color.get())
        
        
class SettingsWindow():
    def __init__(self, window, settings:Settings_t, closeWindow_func=None) -> None:
        
        self.__settingsWindow = Window (    title="Settings",
                                            geometry="500x200",
                                            mainWindow=window)
        self.__tempSettings = Settings_t(   baud=settings.baudrate.get(),
                                            newLine=settings.newLine.get(),
                                            curReturn=settings.curReturn.get())
        
        
        self.closeWindow_func = closeWindow_func
        self.Show()
        
    def Show(self):
        ENTRY_WIDTH = 30
       
        labelBaud = Label(  window=self.__settingsWindow,
                            text="Baudrate:")
        labelBaud.Place(    column=0,
                            row=0,
                            padx=5,
                            pady=5)
        
        entryBaud = Entry(  window=self.__settingsWindow,
                            textvariable=self.__tempSettings.baudrate,
                            width=ENTRY_WIDTH)
        entryBaud.Place(    column=1,
                            row=0,
                            columnspan=2)
         
        newlineCheckBox = tk.Checkbutton(   master=self.__settingsWindow, 
                                            text="NL", 
                                            variable=self.__tempSettings.newLine)
        newlineCheckBox.grid(   column=1, 
                                row=2)
        
        curReturnCheckBox = tk.Checkbutton( master=self.__settingsWindow, 
                                            text="CR", 
                                            variable=self.__tempSettings.curReturn)
        curReturnCheckBox.grid( column=1, 
                                row=3)
        
        addButton = Button( window=self.__settingsWindow,
                            text="OK",
                            command_func=lambda type="save", settings=self.__tempSettings:self.closeWindow_func(type=type, settings=settings),
                            width=10)                  
        addButton.Place(    column=1,
                            row=5,
                            padx="30",
                            pady="30")
        
        addCancelButton = Button(   window=self.__settingsWindow,
                                    text="Cancel",
                                    command_func=lambda type = "cancel":self.closeWindow_func(type=type),
                                    width=10)
        addCancelButton.Place(  column=2,
                                row=5,
                                padx="30",
                                pady="30")
        
        self.__settingsWindow.Show()
        
    def Close(self):
        self.__settingsWindow.Close()