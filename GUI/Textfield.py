# Copyright (C) 2024 Niedl Andreas
#
# This file is part of Serial-Button-GUI.
#
# Serial-Button-GUI is licensed under the GNU General Public License v3.0.
# You can redistribute it and/or modify it under the terms of the GPL-3.0.
#
# See the LICENSE file for details.

from textwrap import wrap
import tkinter as tk
from tkinter import scrolledtext
#from tkinter import scrolledtext

class Textfield(scrolledtext.ScrolledText):
    def __init__(self, window, width, height, font) -> None:

        scrolledtext.ScrolledText.__init__( self,
                                            master=window,
                                            wrap = tk.WORD,
                                            width = width,
                                            height = height,
                                            font = font)
        

    def Place(self, column, row, columnspan, rowspan, padx, pady):
        self.grid(  column=column,
                    row=row,
                    columnspan=columnspan,
                    rowspan=rowspan,
                    padx=padx,
                    pady=pady)
        self.configure(state="disabled")

    def Print(self, text):  
        self.configure(state="normal")
        self.insert(tk.INSERT, text)
        #self.insert(tk.INSERT, "\r\n")
        self.yview(tk.END)
        self.configure(state="disabled")

    def Clear(self):
        self.configure(state="normal")
        self.delete("1.0", tk.END)
        self.configure(state="disabled")

    def Delete(self):
        self.destroy()
        
