# Copyright (C) 2024 Niedl Andreas
#
# This file is part of Serial-Button-GUI.
#
# Serial-Button-GUI is licensed under the GNU General Public License v3.0.
# You can redistribute it and/or modify it under the terms of the GPL-3.0.
#
# See the LICENSE file for details.

import serial
import serial.tools.list_ports
from SERIAL.Receiver import Receiver as rx
from SERIAL.Transmitter import Transmitter as tx
from HELPER.data_types import Settings_t
import time

class SerialPort(serial.Serial):
    def __init__(self, txRepeatTimeVar) -> None:
        super().__init__()
        self.close()
        self.__availablePorts = {}
        self.__tx = tx(self)
        self.__rx = rx(self)
        self.serialState = False
        self.currentRepeatTime = 0.1 #s
        self.newRepeatTime = txRepeatTimeVar
        self.repeatMode = False
        self.lastSend = ""
        self.repeatSendNextTimeToRun = 0

    def __del__(self):
        self.close()
        print("Port is closed...")

    def GetAvailablePorts(self):
        ports = serial.tools.list_ports.comports()
        self.__availablePorts.clear()
        for port, desc, hwid in sorted(ports):
            self.__availablePorts[desc] = port
        return self.__availablePorts

    def GetState(self):
        return self.serialState

    def SetPrinterFunc(self, print_func):
        self.__rx.SetPrinterFunc(print_func)

    def SetPort(self, portName):
        self.close()
        self.port = self.__availablePorts[portName]

    def Open(self):
        self.open()
        print("Port is open...")
        self.serialState = True

    def Close(self):
        self.close()
        print("Port is closed...")
        self.serialState = False

    def Send(self, message):
        self.lastSend = message
        if self.serialState == True:
            try:
                self.__tx.Send(message=self.lastSend)
            except Exception as e:
                self.close()
                
            self.__RepeatSendRestart()
        else:
            print("Serial-Port not open!!!")

    def SetSettings(self, settings:Settings_t):
        self.baudrate = settings.baudrate.get()
        self.__rx.SetSettings(settings=settings)
        self.__tx.SetSettings(settings=settings)
        
    def PollRx(self):
        self.__rx.PollRx()
        
    def SendCancelCommand(self):
        cancel = chr(26) #^Z = STRG + Z
        self.__tx.Send(message=cancel, fixMessage=True)
        
    def SetRepeatSend(self, state):
        if state == True:
            self.repeatMode = True
            self.CheckNewRepeatTime()
        else:
            self.repeatMode = False
        self.lastSend = ""
            
    def CheckNewRepeatTime(self):
        try:
            newRepeatTime = self.newRepeatTime.get()
            newRepeatTime = float(newRepeatTime)
        except:
            return
        if newRepeatTime == self.currentRepeatTime:
            return
        
        if newRepeatTime < 0.1: #check min repeat time
            return
        self.currentRepeatTime = newRepeatTime
        self.__RepeatSendRestart()
        
    def PollRx(self):
        self.__rx.PollRx(port="")
    
    def PollRepeatSend(self):
        
            if time.time() > self.repeatSendNextTimeToRun: # run every x seconds
                self.__RepeatSendRestart()
                
                if self.repeatMode == True and self.lastSend != "":
                    if self.serialState == True:
                        try:
                            self.__tx.Send(message=self.lastSend)
                        except Exception as e:
                            self.SetRepeatSend(state=False)
                            print("Send Serial-Port failed --> Close Port!!! (Error: %s)" % e)
                            self.Close()
                    else:
                        print("Serial-Port not open!!!")
                        
    def __RepeatSendRestart(self):
        self.repeatSendNextTimeToRun = time.time() + self.currentRepeatTime