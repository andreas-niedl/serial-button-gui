# Copyright (C) 2024 Niedl Andreas
#
# This file is part of Serial-Button-GUI.
#
# Serial-Button-GUI is licensed under the GNU General Public License v3.0.
# You can redistribute it and/or modify it under the terms of the GPL-3.0.
#
# See the LICENSE file for details.

from HELPER.data_types import Settings_t

class Receiver():
    def __init__(self, port) -> None:
        self.port = port
        self.printer_func = None
        self.__newLine = False
        self.__curReturn = False

    def PollRx(self, port):
            if self.port.GetState():
                try:
                    input = str(self.port.read_all(), "utf-8")
                    if not len(input) == 0:
                        if not self.printer_func == None:
                            self.printer_func(input)
                except:
                    pass
                
    def SetPrinterFunc(self, print_func):
        self.printer_func = print_func

    def SetSettings(self, settings:Settings_t):
            self.__newLine = settings.newLine.get()
            self.__curReturn = settings.curReturn.get()