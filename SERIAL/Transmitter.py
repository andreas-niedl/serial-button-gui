# Copyright (C) 2024 Niedl Andreas
#
# This file is part of Serial-Button-GUI.
#
# Serial-Button-GUI is licensed under the GNU General Public License v3.0.
# You can redistribute it and/or modify it under the terms of the GPL-3.0.
#
# See the LICENSE file for details.

from HELPER.data_types import Settings_t

class Transmitter():
    def __init__(self, port) -> None:
        self.__port = port
        self.__newLine = False
        self.__curReturn = False

    def Send(self, message, fixMessage = False):
        
            if fixMessage == False:
                if self.__curReturn == True:
                    message = message + "\r"
                if self.__newLine == True:
                    message = message + "\n"
            str_bytes = bytes(message, 'UTF-8')
            self.__port.write(str_bytes)
        
        
    def SetSettings(self, settings:Settings_t):
        self.__newLine = settings.newLine.get()
        self.__curReturn = settings.curReturn.get()